# Description #

A custom API which uses the PHP library of the MessageBird company to send sms (also concatenated)

### Next phase ###

a) Implement a service container and inject the classes using method or conctuctor injection.
b) Rename the class MsgRepository (to something more readable) or Inject a service instead,
   as there's no data to obtain.

### Application structure ###

Index.php => ApiFactory => MessageBirdApi => MsgRepositoryInterface => MsgRepository

### Request type ###

Json post request that contains:

a) Recipient 
b) Message
c) Originator

### Setup ###

composer install
composer dump-autoload -o (all the classes are loaded with psr-4)