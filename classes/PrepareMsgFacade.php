<?php

namespace SmsMessage;

class PrepareMsgFacade
{

    public $messageData;
    protected $udhPrefix = '050003';

    // Validate the data and concatenate the sms if needed
    public function __construct($messageData)
    {
        $this->messageData = $this->ValidateData(json_decode($messageData, true), $this->validationRules());
        $this->concatenatedSms();
    }

    // The validation rules (The phone number is validated differently)
    private function validationRules()
    {
        return array('message' => array('filter' => FILTER_SANITIZE_STRING),
                     'originator' => array('filter' => FILTER_SANITIZE_STRING));
    }

    private function ValidateData($data, $rules) {
        $phNr = $data['recipient'];
        //Validate the message and the originator (wrong characters or empty field wont be validated succesfully)
        $data = filter_var_array($data, $rules);
        if (!in_array(false, $data) || !in_array('', $data))
        {
            //Validate the phone number (length, no dot, no chars)
            if (is_numeric($phNr) && floor($phNr) == $phNr && strlen($phNr) == 11)
            {
                $data['recipient'] = $phNr;
                return $data;
            }
        }
        return false;
    }

    // Concatenate the sms if needed. The buildSmsPart method is called for every part of the concatenated sms
    private function concatenatedSms()
    {
        if ($this->messageData)
        {
            if (strlen($this->messageData['message']) > 160)
            {
                $index = 0;
                $totalMessages = ceil(strlen($this->messageData['message']) / 153);
                $randNr = rand(0, 255);
                for ($i = 1 ; $i <= $totalMessages; $i++) 
                {
                    $smsParts[] = $this->buildSmsPart($totalMessages, $i, $index, $randNr);
                    $index += 153; 
                }
                $this->messageData['message'] = $smsParts;
            }
        }
    }

    private function buildSmsPart($totalMessages, $i, $index, $randNr)
    {
        $subMsg = substr($this->messageData['message'], $index, 153);
        $udh = $this->udhPrefix.$randNr.$totalMessages.$i; 
        //$udh = $this->udhPrefix.dechex($randNr).dechex($totalMessages).dechex($i); 
        return array('message' => $subMsg, 'udh' => $udh);
    }

    public function validationFailMsg()
    {
        return 'Some fields are empty or contain wrong type data';
    }
}