<?php

namespace SmsMessage;
use SmsMessage\PrepareMsgFacade;
use MessageBird\Client;
use SmsMessage\Repositories\MsgRepositoryInterface; 

// with the Repository and it's interface the class is de-coupled 
// from the logic which handles the communication for sending sms.

class MessageBirdApi
{

	private $messageData; // The post data
	private $Message; // The object which contains the prepared (validated, concatinated) message to be sent
	private $MsgRepository; // Containes the logic of the MsgRepository class through the interface injected below

    public function __construct(MsgRepositoryInterface $MsgRepository)
    {
    	if ($this->isRequestPost()) // Check if there is a post request
    	{
    		$this->MsgRepository = $MsgRepository;
    		$this->prepareMsg();
    	}
    }

    // The class to build the sms (Facade pattern)
    private function prepareMsg()
    {
        // Ideally this class should be injected in the constructor (dependency injection)
    	$this->Message = new PrepareMsgFacade($this->messageData);
    }

    public function sendMessage()
    {
    	if ($this->checkBeforeSend()) // Check if everything is ready
    	{
    		$MsgSent = $this->MsgRepository->send($this->Message->messageData); // Send the message
    		$this->response($this->checkAfterSend($MsgSent)); // Get feedback
    	}
    }

    private function checkBeforeSend()
    {
    	//Check if the message is valid and contain no errors
    	if (!$this->Message->messageData)
    	{
    		$this->Response($this->Message->validationFailMsg());
    		return false;
    	}
		//Check if there's enough balance to send messages
    	if (!$this->MsgRepository->checkBalance())
    	{
    		$this->Response($this->MsgRepository->failBalanceMsg());
    		return false;
    	}
    	return true;
    }

    //If the message was sent succesfully and instance of the Message object is returned. Otherwise a string
    //from catching the exception. If the message is concatenated it's an array of cloned instances. If for any
    //reason one "submessage" failed to be sent, the error is shown through the Response method.

    private function checkAfterSend($MsgSent)
    {
    	if (!is_array($MsgSent))
    	{
    		return $MsgSent instanceof \MessageBird\Objects\Message ? 'The message was sent' : $MsgSent;
    	}
    	foreach ($MsgSent as $key => $subMsg) {
    		if (!$subMsg instanceof \MessageBird\Objects\Message)
    		{
    			return $subMsg;
    		}
    	}
    	return  'The message was sent';
    }



    private function isRequestPost()
    {
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST')
        {
        	$this->messageData = file_get_contents("php://input");
        	return true;
        }
       
	   	$this->Response('No Post Request');
	   	return false;
    }

    private function Response($message)
    {
        echo json_encode(array('message' => $message));
    }
}