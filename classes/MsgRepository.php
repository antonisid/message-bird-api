<?php

namespace SmsMessage\Repositories;
use MessageBird\Client;
use MessageBird\Objects\Message;

class MsgRepository implements MsgRepositoryInterface {

	protected $accessKey = 'JT6IJ01o3HFq3kIduxsawIDa7'; 
	private $MessageBird;
	private $Message;

	public function __construct()
    {
    	// Ideally the classes should be injected to the constructor or to the respective methods (next phase)
    	$this->MessageBird = new Client($this->accessKey);
    	$this->Message = new Message();
    }


	public function checkBalance()
	{
		try {
		
			$balance = $this->MessageBird->balance->read();
			if ($balance->amount > 0)
			{
				return true;
			}
			return false;
		} 
		catch (\Exception $e) {
		  echo $e->getMessage();
		}
	}

	public function send($messageData)
	{
		// If there's only one message to be sent (no concatenation)
		if (!is_array($messageData['message']))
		{
			return $this->msgSendRequest($messageData); //Return the response (instance or string)
		}
		foreach ($messageData['message'] as $key => $subMessage) 
		{
			// end every part of the concatenated sms seperately.
			$msgSent = $this->msgSendRequest($messageData, $subMessage); // Either instance of the Message or string
			$subMessages[] = is_object($msgSent) ? clone $msgSent : $msgSent;
		}
		//Return an array of cloned instances or strings
		return $subMessages;
	}

	public function msgSendRequest($messageData, $subMessage = null)
	{
		try 
		{
			$this->Message->originator = $messageData['originator'];
			$this->Message->recipients = array($messageData['recipient']);
			$this->Message->typeDetails = isset($subMessage) ? array('udh' => $subMessage['udh']) : null;
			$this->Message->body = isset($subMessage) ? $subMessage['message'] : $messageData['message'];
			$MessageSent = $this->MessageBird->messages->create($this->Message);
			return $MessageSent;
		}
		catch (\Exception $e) {
		  	return $e->getMessage();
		}
	}

	public function failBalanceMsg()
    {
        return 'The balance is not sufficient to use this service';
    }
}