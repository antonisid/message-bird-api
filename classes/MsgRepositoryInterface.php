<?php
namespace SmsMessage\Repositories;
 
interface MsgRepositoryInterface {

    public function checkBalance();

    public function failBalanceMsg();

    public function send($messageData);

    public function msgSendRequest($messageData);

}