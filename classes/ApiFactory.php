<?php

namespace SmsMessage;
use SmsMessage\Repositories\MsgRepository; 

class ApiFactory
{
    
    public static function create()
    {
    	$MsgRepository = new MsgRepository();
        return new MessageBirdApi($MsgRepository);
    }
}
