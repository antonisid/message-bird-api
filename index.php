<?php

use SmsMessage\ApiFactory;
require_once 'vendor/autoload.php';

// With the factory pattern an instance of the MessageBirdApi is returned.
$message = ApiFactory::create();
$message->sendMessage();
